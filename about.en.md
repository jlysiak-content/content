---
title: "About"
draft: false
aliases: ["/about"]
showToc: false
TocOpen: false
hidemeta: false
metaOnlyTranslationList: true
comments: false
disableShare: true
---
[heniproject]:https://www.mimuw.edu.pl/~iwanicki/projects/heni/
[whip6-pub]:https://github.com/InviNets/whip6-pub.git
[cherrysimac]:https://misc.lysiak.ovh/csmasters/jlysiak-cs-masters.pdf
[cherrysimac-bib]:https://misc.lysiak.ovh/csmasters/jlysiak-cs-masters.bib
[airr-main]:https://thedroneracingleague.com/airr/
[mimotaurspl]:https://mimotaurs.pl
[cv]:https://misc.lysiak.ovh/jlysiak-cv.pdf
[alphapilot]:https://www.herox.com/alphapilot
[alphapilot-leaderboard]:https://www.herox.com/alphapilot/leaderboard
[airr-results]:https://www.herox.com/alphapilot/update/3012
[iypt]:https://2010.iypt.org/en/iypt2010/results_selectives/index.html
[tmf]:http://tmf.org.pl/pl/archiwum/tmf-2010/wyniki/
[sdcn]:https://confirm.udacity.com/YWJZML6J

### Bio
Currently, I work as a software engineer in Arista Networks where we
are brining to life high-performance data center switches. I'm big enthusiast of
Open Source and Open Hardware, particularly interested in low-level programming,
hardware and autonomous systems. I hate boredom, so in my spare time I work on
my own projects, just walk or train.

Since I remember I've always thought that you understand something only when
you can explain it. That's why I was teaching high school students Maths and
Physics for a few years at the beginning of my studies, and now I would like to
share my knowledge with you. I hope you will find this site useful :wink:.

Below, you can find some interesting facts about me in CV-like form.  
However, if you are interested in my short-form CV, you can find it [here][cv].
There are no contact details (for obvious reason), but you can reach me out via
email/Twitter/LinkedIn/etc for full CV or just to have a short discussion.

### Work experience
* Arista Networks (Software Engineer, Dublin, Jan 2021 - present)
* NoMagic (Software Engineer Intern, Warsaw, Sep - Nov 2020)
* Spaceti (Firmware/Embedded Developer, Prague, Apr 2019 - Apr 2020)
* Eracent (Junior Angular Developer, Warsaw, Oct 2017 - May 2018)
* Samsund R&D Poland (Firmware Developer Intern, Jul 2017 - Sep 2017)
* Genesis Mobo (Electronics Designer/Firmware Developer, Jul 2015 - Oct 2016)
* John Pitcher (Electronics Designer/Firmware Developer, Oct 2012 - May 2013)
* Private Physics/Maths Teacher (2012 - 2017)

### Projects/Activities
* [Warsaw MIMotaurs][mimotaurspl] (Team member, Mar 2019 - Dec 2019)
    * I was responsible for camera calibration toolkit and pipline for a fast
      and reliable drone's camera calibration.
    * Prepared race track generator for guidance and navigation subsystem.
    * Trained and tests multiple ML models for image segmentation and checkpoint
      detection.
* [HENI][heniproject] (Student member, University of Warsaw, Apr 2018 - Dec 2019)
    * Implemented MAC layer protocol for the whip6 OS network stack [nesC].
    * Designed and implemented tools for experiments automation and debugging
      which use multi-microprocessor synchronization and code execution tracking
      with logic analyzers [C/Python].
    * Deployed real-world homogeneous IoT experimental setup [(1KT)](https://www.mimuw.edu.pl/~iwanicki/projects/heni/1kt.html) consisting of 1000 low-power wireless nodes.
      

### Achievements
* As a part of Warsaw MIMotaurs team:
    * [4th place][airr-results] in [AIRR 2019][airr-main] World's Championship, Austin 2019
    * [3rd place][alphapilot-leaderboard] in the [AlphaPilot Innovation Challange][alphapilot] (qualification to AIRR)
* Polish Team member, [XXIII International Young Physicists’ Tournament][iypt], Austria 2010
* Team member of winning team, [Turniej Mlodych Fizykow 2010][tmf] (Young Physicists’ Tournament 2010)

### Education
* University of Warsaw, MSc, Computer Science, 2018-2020
* University of Warsaw, MSc, Physics, 2016-2018
* University of Warsaw, BSc, Computer Science, 2014-2018
* University of Warsaw, BSc, Physics, 2013-2016
* Warsaw University of Technology, Eng,  Robotics and Automation Engineering, 2012 – 2013 (aborted)
* Stanisław Staszic XIV High School in Warsaw, Matex, 2009 - 2012

### Publications

- **1KT: A Low-Cost 1000-Node Low-Power Wireless IoT Testbed**
  [[PDF]](https://www.mimuw.edu.pl/~mbanaszek/publications/banaszek-mswim2021.pdf)
  [[BIB]](https://www.mimuw.edu.pl/~mbanaszek/publications/banaszek-mswim2021.bib)  
  *Mateusz Banaszek, Wojciech Dubiel, Jacek Łysiak, Maciej Dębski, Maciej Kisiel, Dawid Łazarczyk,
  Ewa Głogowska, Przemysław Gumienny, Cezary Siłuszyk, Piotr Ciołkosz, Agnieszka Paszkowska,
  Inga Rüb, Maciej Matraszek, Szymon Acedanski, Przemysław Horban, Konrad Iwanicki*  
  MSWiM 2021: Proceedings of the 24th International ACM Conference on Modeling,
Analysis and Simulation of Wireless and Mobile Systems. ACM. Alicante, Spain.
November 2021.

- **An Implementation and Evaluation of a Robust Link Layer for Low-power Wireless Network Protocol Stacks**
  [[PDF]][cherrysimac]
  [[BIB]][cherrysimac-bib]
  *Jacek Łysiak*  
  Master's thesis, University of Warsaw, 12.2020
- **Lessons from Communication Problems that Nearly Jeopardized Development
  of Hardware-Software Support for a 1000-Device IoT Testbed**
  [[PDF]](https://www.mimuw.edu.pl/~mbanaszek/publications/banaszek-failsafe2020.pdf)
  [[BIB]](https://www.mimuw.edu.pl/~mbanaszek/publications/banaszek-failsafe2020.bib)
  [[ACM DL]](https://dl.acm.org/doi/10.5555/3400306.3400357)  
  *Mateusz Banaszek, Inga Rüb, Maciej Dębski, Agnieszka Paszkowska, Maciej Kisiel, Dawid Łazarczyk,
  Ewa Głogowska, Przemysław Gumienny, Cezary Siłuszyk, Piotr Ciołkosz, Jacek Łysiak, Wojciech Dubiel,
  Szymon Acedański, Przemysław Horban, Konrad Iwanicki*  
  EWSN 2020: Proceedings of the 2020 International Conference on Embedded
  Wireless Systems and Networks, FAILSAFE Workshop.
  Junction Publishing / ACM. Lyon, France. February 2020. pp. 259—264.


### Licences/Certifications
* [Self-Driving Car Engineer][sdcn], May 2021
