---
title: "Witaj!"
date: 2021-05-15T11:30:03+00:00
tags: ["general"]
draft: false
# weight: 1
# aliases: ["/first"]
# author: "Me"
showToc: false
TocOpen: false
#hidemeta: false
#comments: false
#description: "Desc Text."
#canonicalURL: "https://canonical.url/to/page"
#disableHLJS: true # to disable highlightjs
#disableShare: false
#disableHLJS: false
#hideSummary: false
#searchHidden: true
#ShowReadingTime: true
#ShowBreadCrumbs: true
#ShowPostNavLinks: true
#cover:
#    image: "<image path/url>" # image path/url
#    alt: "<alt text>" # alt text
#    caption: "<text>" # display caption under cover
#    relative: false # when using page bundles set this to true
#    hidden: true # only hide on current single page
#editPost:
#    URL: "https://github.com/<path_to_repo>/content"
#    Text: "Suggest Changes" # edit text
#    appendFilePath: true # to append file path to Edit link
---

Hej! Miło Cię poznać!

Cieszę się, że udało Ci się tutaj dotrzeć i mam nadzieję, że ta strona będzie
dla Ciebie choć trochę przydatna (w przyszłości :wink:).

Jak na razie jest w budowie, ale mam w planach wrzucać coś na nią w miarę
regularnie.

Jeśli interesuje Cię, w jaki sposób można zbudować coś podobnego, zerknij
na mojego [Gitlab-a](https://gitlab.com/jlysiak-content/website).
