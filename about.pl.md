---
title: "Parę słów o sobie"
draft: false
aliases: ["/pl/omnie"]
showToc: false
TocOpen: false
hidemeta: false
metaOnlyTranslationList: true
comments: false
disableShare: true
---
[heniproject]:https://www.mimuw.edu.pl/~iwanicki/projects/heni/
[whip6-pub]:https://github.com/InviNets/whip6-pub.git
[cherrysimac]:https://misc.lysiak.ovh/csmasters/jlysiak-cs-masters.pdf
[cherrysimac-bib]:https://misc.lysiak.ovh/csmasters/jlysiak-cs-masters.bib
[airr-main]:https://thedroneracingleague.com/airr/
[mimotaurspl]:https://mimotaurs.pl
[cv]:https://misc.lysiak.ovh/jlysiak-cv.pdf
[alphapilot]:https://www.herox.com/alphapilot
[alphapilot-leaderboard]:https://www.herox.com/alphapilot/leaderboard
[airr-results]:https://www.herox.com/alphapilot/update/3012
[iypt]:https://2010.iypt.org/en/iypt2010/results_selectives/index.html
[tmf]:http://tmf.org.pl/pl/archiwum/tmf-2010/wyniki/
[sdcn]:https://confirm.udacity.com/YWJZML6J

### Bio
Aktualnie pracuję jako programista w Arista Networks, gdzie budujemy
wysoko-wydajne switche do centrów danych.
Jestem wielkim entuzjastą Open Source i Open Hardware.
Moje zainteresowania to głównie programowanie
niskopoziomowe, sprzęt/elektronika i systemy autonomoiczne (pozostałych nie
wymieniam, bo lista będzie za długa).
Nienawidzę nudy, ale całe szczęście mam wiele pomysłów, które w wolnej chwili
realizuję :thumbsup:.

Odkąd pamiętam zawsze uważałem, że rozumiemy coś tylko wtedy, gdy potrafimy to
dobrze wytłumaczyć. Dlatego, w trakcie studiów, przez długi czas udzielałem
korepetycji z Matematyki i Fizyki. Aktualnie chciałbym ponownie zacząć się
dzielić moimi doświadczeniami. Mam nadzieję, że ta strona kiedyś komuś się
przyda :wink:.

Poniżej znajduje się kilka faktów o mnie. Ewentualnie, moje
krótkie CV można pobrać [tutaj][cv]. Oczywiście bez danych kontaktowych, ale
zawsze możesz uderzyć do mnie na mail-a, Twitter-a czy LinkedIn-a po pełną
CV-kę albo żeby po prostu podyskutować.

### Doświadczenie zawodowe
* Arista Networks, Programista, Dublin, 01.2021 - aktualnie
* NoMagic, Programista (staż), Warszawa, 09.2020 - 11.2020
* Spaceti, Programista systemów wbudowanych, Praga, 04.2019 - 04.2020
* Eracent, Programista Angular, Warszawa, 10.2017 - 05.2018
* Samsund R&D Polska, Programista systemów wbudowanych (staż), 07.2017 - 09.2017
* Genesis Mobo, Elektronik i programista systemów wbudowanych, 07.2015 - 10.2016
* John Pitcher, Elektronik i programista systemów wbudowanych, 09.2012 - 05.2013
* Korepetytor (Fizyka/Matematyka), 2012 - 2017

### Projekty i aktywności
* [Warsaw MIMotaurs][mimotaurspl], członek zespołu, 03.2019 - 12.2019
    * Przygotowanie narzędzi do szybkiej kalibracji kamer na dronach
        wyścigowych.
    * Napisanie generatora toru wyścigowego dla podsystemu planowania i
        nawigacji.
    * Trenowanie i testowanie modeli do segmentacji obrazów z kamer drona i
      identyfikacji punktów przelowotych.
* [HENI][heniproject], członek zespołu, Uniwersytet Warszawski, 04.2018 - 12.2019
    * Implementacja bezprzewodowego [protokołu MAC][cherrysimac] dla systemu
        operacyjnego whip6 [nesC].
    * Zaprojektowanie i implementacja narzędzi do automatycznych eksperymentów i
        testowania aplikacji wykonywanych na kilku mikroprocesorach z możliwością
        ich synchronozacji i śledzenia wykonania kodu [C/Python].
    * Wdrożenie układu eksperymentalnego dla
      bezprzewodowych sieci niskomocowych [(1KT)](https://www.mimuw.edu.pl/~iwanicki/projects/heni/1kt.html)
      składającego się z 1000-a węzłów.

### Osiągnięcia
* Jako członek drużyny Warsaw MIMotaurs:
    * [IV miejsce][airr-results] w finałowym wyścigu [AIRR 2019][airr-main] , Austin 2019
    * [III miejsce][alphapilot-leaderboard] w konkursie [AlphaPilot Innovation Challange][alphapilot]
        (kwalifikacje do sezonu AIRR)
* Członek drużyny reprezentującej Polskę na [XXIII Międzynarodowym Turnieju Młodych Fizyków][iypt],
   Austria 2010
* Członek zwycięskiej drużyny w [Turnieju Mlodych Fizykow 2010][tmf] (Young Physicists’ Tournament 2010)

### Edukacja
* Uniwersytet Warszawski (MISMaP), Mgr, Informatyka, 2018-2020
* Uniwersytet Warszawski (MISMaP), Mgr, Fizyka, 2016-2018
* Uniwersytet Warszawski (MISMaP), Lic., Informatyka, 2014-2018
* Uniwersytet Warszawski (MISMaP), Lic., Fizyka, 2013-2016
* Warsaw University of Technology, Inż.,  Automatyka i Robotyka, 2012 – 2013
  (przerwane)
* XIV LO im. Stanisława Staszica w Warszawie, Matex, 2009 - 2012

### Publikacje

- **1KT: A Low-Cost 1000-Node Low-Power Wireless IoT Testbed**
  [[PDF]](https://www.mimuw.edu.pl/~mbanaszek/publications/banaszek-mswim2021.pdf)
  [[BIB]](https://www.mimuw.edu.pl/~mbanaszek/publications/banaszek-mswim2021.bib)  
  *Mateusz Banaszek, Wojciech Dubiel, Jacek Łysiak, Maciej Dębski, Maciej Kisiel, Dawid Łazarczyk,
  Ewa Głogowska, Przemysław Gumienny, Cezary Siłuszyk, Piotr Ciołkosz, Agnieszka Paszkowska,
  Inga Rüb, Maciej Matraszek, Szymon Acedanski, Przemysław Horban, Konrad Iwanicki*  
  MSWiM 2021: Proceedings of the 24th International ACM Conference on Modeling,
Analysis and Simulation of Wireless and Mobile Systems. ACM. Alicante, Spain.
November 2021.

- **An Implementation and Evaluation of a Robust Link Layer for Low-power Wireless Network Protocol Stacks**
  [[PDF]][cherrysimac]
  [[BIB]][cherrysimac-bib]
  *Jacek Łysiak*  
  Praca magisterska, Uniwersytet Warszawski, 12.2020
- **Lessons from Communication Problems that Nearly Jeopardized Development
  of Hardware-Software Support for a 1000-Device IoT Testbed**
  [[PDF]](https://www.mimuw.edu.pl/~mbanaszek/publications/banaszek-failsafe2020.pdf)
  [[BIB]](https://www.mimuw.edu.pl/~mbanaszek/publications/banaszek-failsafe2020.bib)
  [[ACM DL]](https://dl.acm.org/doi/10.5555/3400306.3400357)  
  *Mateusz Banaszek, Inga Rüb, Maciej Dębski, Agnieszka Paszkowska, Maciej Kisiel, Dawid Łazarczyk,
  Ewa Głogowska, Przemysław Gumienny, Cezary Siłuszyk, Piotr Ciołkosz, Jacek Łysiak, Wojciech Dubiel,
  Szymon Acedański, Przemysław Horban, Konrad Iwanicki*  
  EWSN 2020: Proceedings of the 2020 International Conference on Embedded
  Wireless Systems and Networks, FAILSAFE Workshop.
  Junction Publishing / ACM. Lyon, France. February 2020. pp. 259—264.

### Certyfikaty
* [Self-Driving Car Engineer][sdcn], 05.2021
