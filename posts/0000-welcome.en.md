---
title: "Welcome!"
date: 2021-05-15T11:30:03+00:00
tags: ["general"]
draft: false
# weight: 1
# aliases: ["/first"]
# author: "Me"
showToc: false
TocOpen: false
#hidemeta: false
#comments: false
#description: "Desc Text."
#canonicalURL: "https://canonical.url/to/page"
#disableHLJS: true # to disable highlightjs
#disableShare: false
#disableHLJS: false
#hideSummary: false
#searchHidden: true
#ShowReadingTime: true
#ShowBreadCrumbs: true
#ShowPostNavLinks: true
#cover:
#    image: "<image path/url>" # image path/url
#    alt: "<alt text>" # alt text
#    caption: "<text>" # display caption under cover
#    relative: false # when using page bundles set this to true
#    hidden: true # only hide on current single page
#editPost:
#    URL: "https://github.com/<path_to_repo>/content"
#    Text: "Suggest Changes" # edit text
#    appendFilePath: true # to append file path to Edit link
---

Welcome! Nice to meet you!

I'm happy that you found my webpage. I hope you will find it at least a bit
useful.

So far, it still has _work in progress_ status but I plan to post some bits of
stuff regularly.

If you are interested in how to build such a webpage, you can go to my
[Gitlab](https://gitlab.com/jlysiak-content/website) and checkout my sources.
